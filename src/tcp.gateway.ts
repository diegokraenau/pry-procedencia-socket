import { HttpService } from '@nestjs/axios';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { lastValueFrom, map } from 'rxjs';

export interface AccidentInfo {
  latitude?: string;
  longitude?: string;
  dateCreated?: string;
}

@WebSocketGateway({ cors: true })
export class TCPGateway {
  constructor(private readonly httpService: HttpService) {}

  @WebSocketServer()
  server;

  @SubscribeMessage('accidents')
  async handleAccidents(@MessageBody() accidentInformation: AccidentInfo) {
    console.log(
      'Entrando a tu llamada y se envia esto - accidents',
      accidentInformation,
    );
    console.log('Enviando a esta ip', `${process.env.API}/api/accidents`);
    const resp = await lastValueFrom(
      this.httpService
        .post(`${process.env.API}/api/accidents`, accidentInformation)
        .pipe(
          map((resp) => {
            return resp.data;
          }),
        ),
    );
    this.server.emit('accidents', resp);
  }

  
  @SubscribeMessage('accidents-taken')
  async handleAccidentsTaken(@MessageBody() data: any) {
    console.log(
      'Entrando a tu llamada y se envia esto - accidents-taken',
      data?.id,
    );
    console.log('Enviando a esta ip: ', `${process.env.API}/api/accidents`);
    const resp = await lastValueFrom(
      this.httpService
        .put(`${process.env.API}/api/accidents/${data?.id}`, {
          status: 1,
          userPolice: data?.userPolice,
        })
        .pipe(
          map((resp) => {
            return resp.data;
          }),
        ),
    );
    this.server.emit('accidents-taken', resp);
  }
}

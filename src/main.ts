import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger();
  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, //Validate body if accept any other field in json
    }),
  );
  // app.setGlobalPrefix('socket');
  await app.listen(process.env.PORT);

  //Debug
  logger.log(`Server rendering in ${await app.getUrl()}`);
  logger.log(`Environment: ${process.env.ENV}`);
}
bootstrap();

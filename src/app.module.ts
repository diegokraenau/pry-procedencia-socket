import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TCPGateway } from './tcp.gateway';

@Module({
  imports: [HttpModule],
  controllers: [AppController],
  providers: [AppService, TCPGateway],
})
export class AppModule {}

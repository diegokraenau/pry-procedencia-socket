const socket = io('http://localhost:3001');

// const socket = io('http://20.114.207.221/');

const inputLatitude = document.getElementById('latitude');
const inputAltitude = document.getElementById('altitude');
const inputDateCreated = document.getElementById('dateCreated');
const inputPlate = document.getElementById('plate');
const inputOwner = document.getElementById('owner');
const inputPhone = document.getElementById('phone');
const inputUser = document.getElementById('user');
const inputAccidentId = document.getElementById('accidentId');
const accidentsList = document.getElementById('accidents');

const handleAccident = () => {
  console.log('usuario', inputUser.value);
  socket.emit('accidents', {
    latitude: inputLatitude.value,
    longitude: inputAltitude.value,
    dateCreated: inputDateCreated.value,
    plate: inputPlate.value,
    owner: inputOwner.value,
    phone: inputPhone.value,
    user: inputUser.value,
  });
};

const takeAccident = () => {
  socket.emit('accidents-taken', {
    id: inputAccidentId.value,
  });
};

socket.on('accidents', (data) => {
  const li = document.createElement('li');
  li.appendChild(
    document.createTextNode(
      `Latitud: ${data?.latitude}  Altitud: ${data?.altitude}  Fecha: ${data?.dateCreated}`,
    ),
  );
  accidentsList.appendChild(li);
});

socket.on('accidents-taken', (data) => {
  console.log('Se debe eliminar del mapa', data?.id);
});
